#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'UB Linux Users Group'
SITENAME = 'UB Linux Users Group'
SITEURL = 'http://ublinux.org'

PATH = 'content'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

THEME = 'pelican-mg'

DISPLAY_PAGES_ON_MENU = False

PATH = 'content'
STATIC_PATHS = ['images', 'pdfs']
OUTPUT_PATH = 'public'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
#CATEGORY_FEED_ATOM = 'feeds/{category}.atom.xml'
#AUTHOR_FEED_ATOM = 'feeds/{author}.atom.xml'

SITESUBTITLE = ''


# Social widget
SOCIAL = (('envelope', 'mailto:mail@ublinux.org'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Settings for mg theme
TAG_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
DIRECT_TEMPLATES = ('index', 'categories', 'archives',
                    'search', 'tipue_search')
TIPUE_SEARCH_SAVE_AS = 'tipue_search.json'
