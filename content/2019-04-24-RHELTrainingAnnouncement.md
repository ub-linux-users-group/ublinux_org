Title: RHCSA Training Announcement
Date: 2019-04-24 19:20
Category: Announcement
Tags: meeting, announcement, calendar
Author: Christopher Stafford
Summary: Announcement for RHCSA study group on 2019-04-28

April 28th 1400-1600
The Residences At Annapolis Junction Conference Room
10125 Junction Drive, Annapolis Junction, MD 20701

On 28 April 2019 The UB Linux Users group will be hosting a study session for the RHCSA.
All are invited to attend, we will each be preforming the tasks required to pass the test in a group
setting so we can all lean on each other for help.

Required Items:

 - Laptop w/ 2 Centos VMs
 - 1 Gui Install
 - 1 Minimal Install


For more information please email [chris@chrisstafford.io](mailto:chris@chrisstafford.io)