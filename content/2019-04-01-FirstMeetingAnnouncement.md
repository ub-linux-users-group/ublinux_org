Title: First Meeting Announcement
Date: 2019-04-01 18:55
Category: Announcement
Tags: meeting, announcement, calendar
Author: Christopher Stafford
Summary: Announcement for first meeting on 2019-04-07

The inaugural meeting will take place on April 7th 2019 at 2:00pm

Topics:

 - Welcome!
 - What is a Distro?
 - Installfest

For the inaugural meeting we will go over what the goals of the Linux Users Group
are, along with an overview of what exactly a Linux Distro is.


After the presentations, experienced users will assist new users installing the distro
of their choice. We will be providing install images for Ubuntu, Fedora, and Manjaro.


All attendees are encouraged to bring their laptops and blank USB flash drive.


For more information please email [chris@chrisstafford.io](mailto:chris@chrisstafford.io)
