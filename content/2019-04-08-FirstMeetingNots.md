Title: Meeting 001 Notes
Date: 2019-04-08 17:39
Category: Notes
Tags: meeting, notes
Slug: 001-notes
Author: Christopher Stafford
Summary: Meeting notes for 2019-04-07

In the first meeting of the UBLUG we discussed what a linux distro is, and what makes them different. After the presentations we had an installfest and helped five members install linux on their laptops.

For those who couldn't make it but would like help installing Linux on their own machines, please contact Chris and request access to our telegram group, and ask for help! USB install drives and assistance will also be availble at our next meetup.

Slides and links from the first meeting below:

 - [Welcome Presentation]({static}/pdfs/Welcome.pdf)
 - [What is a Distro?]({static}/pdfs/WhatIsADistro.pdf)
 - [Choose Linux Podcast](https://chooselinux.show)
 - [Linux Unplugged Podcast](https://linuxunplugged.com)
